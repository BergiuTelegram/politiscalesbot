const konst = `<b>Konstruktivismus / Essentialismus</b>

Warum sind Menschen so wie sie sind? Diese Achse erlaubt ihnen, sich zwischen zwei gegenseitigen Polen zu platzieren, welche diese Frage beantworten.

Die <b>Konstruktivisten</b> schätzen, dass ein Individuum sich an seinem Umfeld (besonders dem sozialem) sozusagen konstruiert, und dass die Charakteristiken, die es ausmachen, erworben sind.

Dagegen sind die <b>Essentialisten</b> der Meinung, dass Gene und Natur ein Individuum zu dem machen, das es ist. Das bedeutet, dass die definierenden Eigenschaften einer Person angeboren sind.`;

const reso = `<b>Resozialisierungsjustiz / Vergeltungsjustiz</b>

Die Anhänger der <b>Resozialisierungsjustiz</b> sind überzeugt, Urteile sollten nicht dafür gedacht sein, Menschen zu verletzen, sondern den Verurteilten zu helfen, indem die Justiz sie auf den rechten Weg begleitet.

Stattdessen behaupten die Anhänger der <b>Vergeltungsjustiz</b>, die Urteile sollten in erster Linie ein Abschreckungsmittel sein, um Verbrechen zu verhindern. So schafft man, laut den Befürwortern, ein System welches den Verbrechern hilft indem es sie vom Rezidivismus abschreckt, und zugleich den braven Bürgern hilft, da weniger sich trauen, Straftaten zu begehen.`;

const prog = `<b>Progressivismus / Konservatismus</b>

Die <b>Progressisten</b> streben immer nach sozialen Fortschritt, um die Gesellschaft zu verbessern. Sie achten wenig auf Tradition und finden, dass es Heute besser ist als damals, und es in Zukunft vielleicht noch besser sein könnte.

Die <b>Konservativen</b> bevorzugen den sog. Status Quo. Manchmal fordern sie sogar eine Rückkehr zu (oder den Schutz von) bestimmten Werten, die bedroht sind oder in der Vergangenheit liegen. Diese können religiös, oder auch historisch geprägt sein.`;

const inte = `<b>Internationalismus / Nationalismus</b>

Der <b>Internationalismus</b> beinhaltet viele Ideenströmungen, die in bestimmten Zielen übereinstimmen: Kooperation zwischen Ländern, ohne das eigene Land vorteilhaft zu sehen und behandeln. Die Menschen und Bevölkerungen verschiedener Nationen werden nicht hierarchisch gesehen, sondern als gleich wichtig. Eine der extremeren Ideen ist die Forderung mancher Internationalisten, eine alle Grenzen vollständig abzuschaffen.".

Die <b>Nationalisten</b> streben nach dem Wohlsein des eigenen Staates und dessen Bevölkerung. Sie streben nach der Erhaltung einer nationalen Identität. Ihre Interessen sind national zentriert und rechtfertigen die Schaffung eines Staates für ein bestimmtes Volk.`;

const komm = `<b>Kommunismus / Kapitalismus</b>

Der <b>Kommunismus</b> ist ein sehr politisch breites Feld. In diesem Test illustriert dieser Wert nur, dass Sie dafür sind, die Produktionsmittel ins öffentliche Eigentum zurückzukehren, sei es staatlich oder kollektiv.

Der <b>Kapitalismus</b> ist ebenfalls ein ambivalentes Konzept. In diesem Test bedeutet es nur, dass Sie gegen die Aufhebung des Privatseingentum der Produktionsmittel sind.`;

const markt = `<b>Marktregulierung / Laissez-faire (Libertarismus)</b>

Diese Achse stellt dar, welche die ideale wirtschaftliche Haltung des Staates in ihrer Meinung wäre, wenn es um die Marktwirtschaft geht. In diesem Modell wird angenommen, dass die Produktionsmittel zum Großteil privatisiert sind. Liberale und Keynesians streiten sich darüber, welche Methoden notwendig sind, wobei sie Kapitalismus niemals in Frage stellen. Wenn Sie eher für den Kapitalismus sind, deutet diese Achse an, welche Vorgehensweise sie bevorzugen. Wenn sie ein Kommunist sind, dann zeigt diese Achse lediglich, welche der beiden Vorgehensweisen sie als weniger schädlich sehen.

Die <b>Marktregulierung</b> setzt eine gewisse Regulierung der Wirtschaft durch den Staat voraus, weil jedes Individuum von der Wirtschaft profitieren dürfen und können sollte. Befürworter glauben, man könne dies erreichen, indem man Maßnahmen wie zum Beispiel Planung, Gesetze, Subventionen oder auch anpassbare Steuern durchsetzt.

Dagegen ist das sog. <b>laissez-faire</b> eine wirtschaftliche Doktrine, die ausdrücklich gegen die Einschreitung des Staates in der Wirtschaft ist, weil diese von selbst her auf das gemeinsame Interesse abzielt. Praktisch verlangen Befürworter Sachen wie eine schwachen Gesetzgebung, niedrige Steuern, bis hin zur Abschaffung der Rolle des Staates in der Wirtschaft.`;

const oeko = `<b>Ökologismus / Produktivismus</b>

Ökologische Politik bevorzugt den Schutz der Umwelt, indem sie versucht die Auswirkungen der menschlichen Aktivitäten auf die Artenvielfalt so weit wie möglich zu begrenzen, auch wenn das bedeutet unsere aktuelle Lebensweise zu verändern.

Der <b>Produktivismus</b> kümmert sich hauptsächlich um die menschlichen Bedürfnisse. Der Produktivismus lässt sich anhand des Verlangens nach einer Steigerung der Produktion oder der Benutzung von technischen Mitteln erkennen, welche die Gefährdung oder Zerstörung der Umwelt in Kauf nehmen.`;

const revo = `<b>Revolutionär / Reformismus</b>

Die <b>Revolutionären</b> meinen, Direkte Aktion wäre der beste Weg, die politische Ordnung neu und radikal umzugestalten. Diese Aktionen sind oft am Rande der Legalität, und werden von den gegenwärtigen Institutionen kaum akzeptiert.

Die <b>Reformisten</b> wollen legal und Schritt für Schritt die Gesellschaft umwandeln. Dafür nutzen sie die gegebenen Institutionen, Wahlen, Gesuche, und legale Demonstrationen.`;

const help_msg = `This bot is based on <a href="http://politiscales.net/">politiscales.net</a>.

You can start a test with /start.

<b>Help Commands</b>:
<i>Konstruktivismus / Essentialismus</i>
/help_konstruktivismus
/help_essentialismus

<i>Resozialisierungsjustiz / Vergeltungsjustiz</i>
/help_resozialisierungsjustiz
/help_vergeltungsjustiz

<i>Progressivismus / Konservatismus</i>
/help_proggressivismus
/help_konservatismus

<i>Internationalismus / Nationalismus</i>
/help_internationalismus
/help_nationalismus

<i>Kommunismus / Kapitalismus</i>
/help_kommunismus
/help_kapitalismus

<i>Marktregulierung / Laissez-faire (Libertarismus)</i>
/help_marktregulierung
/help_lessezfaire
/help_libertarismus

<i>Ökologismus / Produktivismus</i>
/help_oekologismus
/help_produktivismus

<i>Revolutionär / Reformismus</i>
/help_revolutionaer
/help_reformismus`;

module.exports = {
    konst,
    reso,
    prog,
    inte,
    komm,
    markt,
    oeko,
    revo,
    help_msg
};
