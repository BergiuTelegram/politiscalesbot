// PolitiScalesCli  Copyright (C) 2019  Bergiu
// Licensed under AGPLv3

var questions_mod = require("./questions.js");
var questions = questions_mod.questions;
var calc = require("./calculate.js");
var small = require("./small_commands.js");
var users = require("./user.js");

selection = ["Stimme voll zu", "Stimme etwas zu", "Weder noch", "Eher dagegen", "Vollkommene Ablehnung"];

function main() {
    var Telegraf = require('telegraf');
    var bot = new Telegraf(process.env.BOT_TOKEN);
    bot.command('start', (ctx) => cmd_start(ctx));
    bot.command('license', (ctx) => small.cmd_license(ctx));
    bot.command('help', (ctx) => small.cmd_help(ctx));
    bot.command('1', (ctx) => cmd_answer(ctx, 0));
    bot.command('2', (ctx) => cmd_answer(ctx, 1));
    bot.command('3', (ctx) => cmd_answer(ctx, 2));
    bot.command('4', (ctx) => cmd_answer(ctx, 3));
    bot.command('5', (ctx) => cmd_answer(ctx, 4));
    bot.command('0', (ctx) => cmd_prev(ctx));
    bot.command('help_konstruktivismus', (ctx) => small.cmd_help_konst(ctx));
    bot.command('help_essentialismus', (ctx) => small.cmd_help_konst(ctx));
    bot.command('help_resozialisierungsjustiz', (ctx) => small.cmd_help_reso(ctx));
    bot.command('help_vergeltungsjustiz', (ctx) => small.cmd_help_reso(ctx));
    bot.command('help_proggressivismus', (ctx) => small.cmd_help_prog(ctx));
    bot.command('help_konservatismus', (ctx) => small.cmd_help_prog(ctx));
    bot.command('help_internationalismus', (ctx) => small.cmd_help_inte(ctx));
    bot.command('help_nationalismus', (ctx) => small.cmd_help_inte(ctx));
    bot.command('help_kommunismus', (ctx) => small.cmd_help_komm(ctx));
    bot.command('help_kapitalismus', (ctx) => small.cmd_help_komm(ctx));
    bot.command('help_marktregulierung', (ctx) => small.cmd_help_markt(ctx));
    bot.command('help_lessezfaire', (ctx) => small.cmd_help_markt(ctx));
    bot.command('help_libertarismus', (ctx) => small.cmd_help_markt(ctx));
    bot.command('help_oekologismus', (ctx) => small.cmd_help_oeko(ctx));
    bot.command('help_produktivismus', (ctx) => small.cmd_help_oeko(ctx));
    bot.command('help_revolutionaer', (ctx) => small.cmd_help_revo(ctx));
    bot.command('help_reformismus', (ctx) => small.cmd_help_revo(ctx));
    bot.launch();
}

function init_question(user)
{
    var text = "Frage %num% von %sum%".replace("%num%", (user.qn + 1)).replace("%sum%", questions.length);
    var question = users.get_question(user, user.qn).question;
    text += "\n" + question +"\n";
    for (var i in selection) {
        var y = i;
        y++;
        text += "\n/" + y +" "+selection[i];
    }
    if (user.qn != 0)
    {
        text += "\n" + "\n/0 Zurück";
    }
    return text;
}

function start(ctx) {
    var user = users.create_user(ctx);
    text = init_question(user);
    return text;
}

function cmd_start(ctx) {
    var text = start(ctx);
    var promise = ctx.reply(text);
    // TODO update der letzten nachricht
    // var user = users.get_user(ctx);
    // promise.then(function(msg) {
    //     user.last_message = msg.message_id;
    // });
}

function prev(ctx) {
    var text;
    var from = ctx.update.message.from;
    var chat = ctx.update.message.chat;
    var user = users.get_user(ctx);
    if (user == null) {
        text = start(ctx);
    } else {
        user = users.get_user(ctx);
        if (user.qn == 0)
        {
            text = "Zurück nicht möglich!";
            text += "\n" + start(ctx);
        } else {
            text = "Zurück!";
            user.qn--;
            text += "\n" + init_question(user);
        }
    }
    return text;
}

function cmd_prev(ctx) {
    var text = prev(ctx);
    ctx.reply(text);
}

function cmd_answer(ctx, ans) {
    var nums = [1, 2/3, 0, -2/3, -1];
    var num = nums[ans];
    var from = ctx.update.message.from;
    var chat = ctx.update.message.chat;
    var user = users.get_user(ctx);
    if (user == null) {
        cmd_start(ctx);
        return;
    }
    var text = selection[ans] + "\n";
    var end = questions.length-1;
    if (user.qn > end) {
        text += "\n" + calc.results(user);
    } else if (user.qn >= end ) {
        users.set_answer(user, user.qn, num);
        user.qn++;
        text += "\n" + calc.results(user);
    } else {
        users.set_answer(user, user.qn, num);
        user.qn++;
        text += "\n" + init_question(user);
    }
    ctx.reply(text);
}

main();

// ctx.editMessageText
