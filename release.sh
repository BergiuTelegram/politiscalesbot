version=$(grep version package.json | cut -d '"' -f4)

docker login registry.gitlab.com

docker build -t politiscalesbot .

docker tag politiscalesbot registry.gitlab.com/bergiutelegram/politiscalesbot:latest
docker tag politiscalesbot "registry.gitlab.com/bergiutelegram/politiscalesbot:v$version"

docker push registry.gitlab.com/bergiutelegram/politiscalesbot:latest
docker push "registry.gitlab.com/bergiutelegram/politiscalesbot:v$version"
