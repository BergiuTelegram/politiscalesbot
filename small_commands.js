var help = require("./help.js");

function cmd_license(ctx) {
    ctx.replyWithMarkdown("This bot is licensed under [AGPLv3](https://gitlab.com/BergiuTelegram/politiscalesbot/blob/master/LICENSE). Fork it on [gitlab.com/BergiuTelegram/politiscalesbot](https://gitlab.com/BergiuTelegram/politiscalesbot)");
}

function cmd_help(ctx) {
    ctx.replyWithHTML(help.help_msg);
}

function cmd_help_konst(ctx) {
    ctx.replyWithHTML(help.konst);
}

function cmd_help_reso(ctx) {
    ctx.replyWithHTML(help.reso);
}

function cmd_help_prog(ctx) {
    ctx.replyWithHTML(help.prog);
}

function cmd_help_inte(ctx) {
    ctx.replyWithHTML(help.inte);
}

function cmd_help_komm(ctx) {
    ctx.replyWithHTML(help.komm);
}

function cmd_help_markt(ctx) {
    ctx.replyWithHTML(help.markt);
}

function cmd_help_oeko(ctx) {
    ctx.replyWithHTML(help.oeko);
}

function cmd_help_revo(ctx) {
    ctx.replyWithHTML(help.revo);
}

module.exports = {
    cmd_license,
    cmd_help,
    cmd_help_konst,
    cmd_help_reso,
    cmd_help_prog,
    cmd_help_inte,
    cmd_help_komm,
    cmd_help_markt,
    cmd_help_oeko,
    cmd_help_revo,
};
