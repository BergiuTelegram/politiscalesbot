FROM node:8

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY *.js ./

CMD ["npm", "start"]

