// License and Copyright notice:
// Copyright (c) 2017 8values. http://8values.github.io.
// https://github.com/RadicaliseesSurInternet/politiscales/blob/master/LICENSE
// Changes are relicensed under GPLv3

var qu = require("./questions.js");
var users = require("./user.js");

function calc_score(score,max_value)
{
    return (100*(score)/(max_value)).toFixed(0);
}

function results(user)
{
    var axes = {};
    for(var i=0; i<qu.questions.length; i++)
    {
        // var question = qu.questions[i];
        var question = users.get_question(user, i);
        // var answer = user.answers[i];
        var answer = users.get_answer(user, i);
        for(var j=0; j<question.valuesYes.length; j++)
        {
            a = question.valuesYes[j];
            if(!(a.axis in axes))
            {
                axes[a.axis] = {
                    val: 0,
                    sum: 0
                };
            }
            if(answer > 0)
            {
                axes[a.axis].val += answer * a.value;
            }
            axes[a.axis].sum += Math.max(a.value, 0);
        }
        for(j=0; j<question.valuesNo.length; j++)
        {
            a = question.valuesNo[j];
            if(!(a.axis in axes))
            {
                axes[a.axis] = {
                    val: 0,
                    sum: 0
                };
            }
            if(answer < 0)
            {
                axes[a.axis].val -= answer * a.value;
            }
            axes[a.axis].sum += Math.max(a.value, 0);
        }
    }
    url = "";
    for(var aK in axes)
    {
        if(axes[aK].val > 0)
        {
            if(url != "")
                url += "&";
            url += aK+"="+calc_score(axes[aK].val, axes[aK].sum);
        }
    }
    url = "/de_DE/results?"+url;
    url = "http://politiscales.net"+url;
    var text = "Dein Ergebnis: "+url;
    return text;
}

module.exports = {
    results: results
};
