var questions_mod = require("./questions.js");
var questions = questions_mod.questions;
var shuffled_indices = questions_mod.shuffled_indices;

var users = {};

function User(userid, chatid) {
    this.userid = userid;
    this.chatid = chatid;
    this.qn = 0;
    this.answers = [];
    this.questions_order = shuffled_indices();
}

function get_question(user, i) {
    var num = user.questions_order[i];
    return questions[num];
}

function get_answer(user, i) {
    var num = user.questions_order[i];
    return user.answers[num];
}

function set_answer(user, i, value) {
    var num = user.questions_order[i];
    user.answers[num] = value;
}

function get_key(ctx) {
    var from = ctx.update.message.from;
    var chat = ctx.update.message.chat;
    return from.id + " " + chat.id;
}

function create_user(ctx) {
    var from = ctx.update.message.from;
    var chat = ctx.update.message.chat;
    var user = new User(from.id, chat.id);
    users[get_key(ctx)] = user;
    return user;
}

function get_user(ctx) {
    if (!(get_key(ctx) in users)) {
        return null;
    }
    var user = users[get_key(ctx)];
    return user;
}

module.exports = {
    User,
    get_question,
    get_answer,
    set_answer,
    create_user,
    get_user
};
